import requests
import json

class ApiClient:
    def __init__(self, auth_key, base_uri):
        self.auth_key = auth_key
        self.base_uri = base_uri

    def get_datastream_list(self):
        r = requests.get(self.base_uri + 'datasets.json/?auth_key=' + self.auth_key)
        content = json.loads(r.text)
        return content

    def datastream(self, guid):
        return DataStream(guid, self.auth_key, self.base_uri)

class DataStream:
    def __init__(self, guid, auth_key, base_uri):
        self.guid = guid
        self.auth_key = auth_key
        self.base_uri = base_uri

    def get_info(self):
        r = requests.get(self.base_uri + 'datasets/' + self.guid + '.json/?auth_key=' + self.auth_key)
        content = json.loads(r.text)
        return content

    def get_response(self, limit=4, page=1):
        resource_uri = self.base_uri
        resource_uri += 'datastreams/' + self.guid
        resource_uri += '/data.json/?auth_key='
        resource_uri += self.auth_key
        resource_uri += '&limit=' + str(limit + 1)
        resource_uri += '&page=' + str(page)
        r = requests.get(resource_uri)
        return DataResponse(r)

class DataResponse:
    def __init__(self, response):
        self.response = json.loads(response.text)

    def get_title(self):
        return self.response['title']

    def get_headers(self):
        headers = []

        for e in self.response['result']['fArray']:
            if 'fHeader' not in e:
                break

            if e['fHeader']:
                headers.append(e['fStr'])

        return headers

    def get_data(self):
        headers = self.get_headers()
        length = len(headers)

        row = []
        data = []

        for e in self.response['result']['fArray']:
            if 'fStr' in e:
                if e['fStr'] in headers:
                    continue

            if 'fNum' in e:
                row.append(e['fNum'])
            elif 'fStr' in e:
                row.append(e['fStr'])

            if len(row) == length:
                row_data = {}
                for i in range(length):
                    row_data[headers[i]] = row[i]

                data.append(row_data)
                row = []

        return data
